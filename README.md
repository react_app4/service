# Сервис

Требования
------------

- [Ansible core](https://docs.ansible.com) 2.9.6

## Клонирование репозитория

```sh
$ git clone git@gitlab.com:skillbox38/service.git
$ cd service/
```

## Ansible
Для дальнейшей работы необходимо заполнить файл `hosts` с IP-адресами, полученными при развертывании окружения.

#### Запуск плейбука

```sh
$ ansible-playbook -b app.yml 
